sulincix-init
=======

An alternative init system written by sulincix (Ali Rıza KESKİN)

Installing
======

.. code-block:: shell
	
	make install DESTDIR=*yourdestdir*

Usage
====

.. code-block:: shell
	
	initctl [enable/disable/start/restart/stop] [name]
	
more info: visit https://gitlab.com/sulinos/devel/sulincix-init/-/tree/master/doc

LICENSING
=====

 sulincix-init is an alternative init system
    Copyright (C) 2020  sulinos / Sulin Devel

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

