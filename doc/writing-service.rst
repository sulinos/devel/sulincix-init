    sulincix-init
    Copyright (C) 2020  sulinos / Sulin Devel

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Writing a service
==========

All service files are bash script. We need 3 functions: **_start** **_stop** **_restart** and **NAME** value.

for example:

.. code-block:: shell

	NAME=hello
	_start(){
		echo "Hello World"
	}
	_stop(){
		echo "stop hello"
	}
	_restart(){
		echo "restart hello"
	}
	
Runlevels
^^^^^^

1. 7 runlevels are avaiable. Starting order is  *0 -> 1 -> ... -> 6  -> 7* and Stopping order is *7 -> 6 -> ... -> 1 -> 0*

2. Services called alphabetically in same runlevels.

3. Service files stored in **/lib/init/services/** and suffix must be xxx.initd

